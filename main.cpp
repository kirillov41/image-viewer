#include <QApplication>
#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QPushButton>
#include <QFileDialog>
#include <QGraphicsBlurEffect>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget* window = new QWidget;

    window->resize(500,500);
    QVBoxLayout* layout = new QVBoxLayout(window);
    QLabel* image = new QLabel;
    QSlider* slider = new QSlider(Qt::Horizontal);
    slider->setMinimum(0);
    slider->setMaximum(10);
    QPushButton* button = new QPushButton("Open");

    layout->addWidget(image);
    layout->addWidget(slider);
    layout->addWidget(button);

    QObject::connect(button, &QPushButton::clicked, [image](){
        image->setPixmap(QFileDialog::getOpenFileName(nullptr,"","","*.jpg *.bmp"));
        image->setScaledContents( true );
        image->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
    });

    QObject::connect(slider, &QSlider::valueChanged,[slider,image](){
        QGraphicsBlurEffect* blur = new QGraphicsBlurEffect;
        blur->setBlurRadius(slider->value());
        image->setGraphicsEffect(blur);
    });


    window->show();
    return a.exec();
}
